## 2.0.3

Fixed a possible race condition when persistent window position is written to file.

Windows with persistent position now save position before hiding the window (not just before closing it).

## 2.0.2

Fixed issue where init waiter would never return in there was an error wired into `Initialize.vi`

## 2.0.1

Updated Readme and Relinked VIs

# 2.0.0

Port from VIPM Version
